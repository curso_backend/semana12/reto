from django import forms
from .models import *

class AutorForm(forms.ModelForm):
    class Meta:
        model = Autor
        fields = ['nombre', 'apellidos', 'nacionalidad', 'descripcion']
        labels = {
            "nombre": "Nombre Autor",
            "apellidos": "Apellidos Autor",
            "nacionalidad": "Nacionalidad Autor",
            "descripcion": "Descripción Autor",
        }

class EditorialForm(forms.ModelForm):
    class Meta:
        model = Editorial
        fields = ['nombre']
        labels = {
            "nombre": "Nombre Editorial",
        }

class LibroForm(forms.ModelForm):
    class Meta:
        model = Libro
        fields = ['titulo', 'fecha_publicacion', 'autor_id', 'editorial_id', 'imagen']
        labels = {
            "titulo": "Título Libro",
            "fecha_publicacion": "Fecha Publicación Libro",
            "autor_id": "Autor(es) Libro",
            "editorial_id": "Editorial(es) Libro",
            "imagen": "Imagen Libro",
        }

class AlquilerForm(forms.ModelForm):
    class Meta:
        model = Alquiler
        fields = ['usuario_id', 'libro_id', 'fecha_devolucion']
        labels = {
            "usuario_id": "Usuario Alquiler",
            "libro_id": "Libro Alquiler",
            "fecha_devolucion": "Fecha Devolución Alquiler",
        }
