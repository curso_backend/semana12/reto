from django.db import models


# Create your models here.
class Autor(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=200, blank=False, null=False)
    apellidos = models.CharField(max_length=220, blank=False, null=False)
    nacionalidad = models.CharField(max_length=100, blank=False, null=False)
    descripcion = models.TextField(blank=False, null=False)
    fecha_creacion = models.DateField('Fecha Creación', auto_now=True, auto_now_add=False)

    class Meta:
        verbose_name = 'Autor'
        verbose_name_plural = 'Autores'
        ordering = ['nombre']

    def __str__(self):
        return f'{self.nombre} {self.apellidos}'

class Editorial(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField('Nombre', max_length=255, blank=False, null=False)
    fecha_creacion = models.DateField('Fecha Creación', auto_now=True, auto_now_add=False)

    class Meta:
        verbose_name = 'Editorial'
        verbose_name_plural = 'Editoriales'
        ordering = ['nombre']

    def __str__(self):
        return self.nombre

class Libro(models.Model):
    id = models.AutoField(primary_key=True)
    titulo = models.CharField('Titulo', max_length=255, blank=False, null=False)
    fecha_publicacion = models.DateField('Fecha Publicación', blank=False, null=False)
    autor_id = models.ManyToManyField(Autor)
    editorial_id = models.ManyToManyField(Editorial)
    imagen = models.ImageField(verbose_name='Imagen', upload_to='libro')
    fecha_creacion = models.DateField('Fecha Creación', auto_now=True, auto_now_add=False)

    class Meta:
        verbose_name = 'Libro'
        verbose_name_plural = 'Libros'
        ordering = ['titulo']

    def __str__(self):
        return self.titulo

class Alquiler(models.Model):
    id = models.AutoField(primary_key=True)
    usuario_id = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    libro_id = models.ForeignKey('Libro', on_delete=models.CASCADE)
    fecha_alquiler = models.DateField('Fecha Alquiler', auto_now=True, auto_now_add=False)
    fecha_devolucion = models.DateField()

    class Meta:
        verbose_name = 'Alquiler'
        verbose_name_plural = 'Alquileres'
        ordering = ['fecha_devolucion']

    def __str__(self):
        return f'Alquiler: {self.id}, Libro: {self.libro_id}, Fecha Devolución: {self.fecha_devolucion}'
