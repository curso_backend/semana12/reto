from django.urls import path
from .views import *

urlpatterns = [
    #Autor
    path('listar_autores/', listar_autores, name='listar_autores'),
    path('crear_autor/', crear_autor, name='crear_autor'),
    path('actualizar_autor/<int:autor_id>', actualizar_autor, name='actualizar_autor'),
    path('eliminar_autor/<int:autor_id>', eliminar_autor, name='eliminar_autor'),

    #Editorial
    path('listar_editoriales/', listar_editoriales, name='listar_editoriales'),
    path('crear_editorial/', crear_editorial, name='crear_editorial'),
    path('actualizar_editorial/<int:editorial_id>', actualizar_editorial, name='actualizar_editorial'),
    path('eliminar_editorial/<int:editorial_id>', eliminar_editorial, name='eliminar_editorial'),

    #Libro
    path('listar_libros/', listar_libros, name='listar_libros'),
    path('crear_libro/', crear_libro, name='crear_libro'),
    path('actualizar_libro/<int:libro_id>', actualizar_libro, name='actualizar_libro'),
    path('eliminar_libro/<int:libro_id>', eliminar_libro, name='eliminar_libro'),

    #Alquiler
    path('listar_alquileres/', listar_alquileres, name='listar_alquileres'),
    path('crear_alquiler/', crear_alquiler, name='crear_alquiler'),
    path('actualizar_alquiler/<int:alquiler_id>', actualizar_alquiler, name='actualizar_alquiler'),
    path('eliminar_alquiler/<int:alquiler_id>', eliminar_alquiler, name='eliminar_alquiler'),
]
