from django.shortcuts import render, redirect
from django.views.decorators.http import require_http_methods
from .forms import *
from .models import *


# Create your views here.
def home(request):
    return render(request, 'libro/index.html')

#Autor
@require_http_methods(['GET'])
def listar_autores(request):
    autores = Autor.objects.all()
    return render(request, 'libro/listar_autor.html', {
        'autores': autores
    })

@require_http_methods(["POST", "GET"])
def crear_autor(request):
    if request.method == 'POST':
        autor_form = AutorForm(request.POST)
        if autor_form.is_valid():
            autor_form.save()
            return redirect('libro:listar_autores')
    else:
        autor_form = AutorForm()
    return render(request, 'libro/crear_actualizar_autor.html', {
        'autor_form': autor_form
    })

@require_http_methods(["POST", "GET"])
def actualizar_autor(request, autor_id):
    autor_found = Autor.objects.get(id = autor_id)
    if request.method == 'GET':
        autor_form = AutorForm(instance = autor_found)
    else:
        autor_form = AutorForm(request.POST, request.FILES, instance = autor_found)
        if autor_form.is_valid():
            autor_form.save()
        return redirect('libro:listar_autores')
    return render(request, 'libro/crear_actualizar_autor.html', {
        'autor_form': autor_form
    })

def eliminar_autor(request, autor_id):
    autor_found = Autor.objects.get(id = autor_id)
    if request.method == 'GET':
        return render(request, 'libro/eliminar_autor.html', {
        'autor_found': autor_found
        })
    if request.method == 'POST':
        autor_found.delete()
        return redirect('libro:listar_autores')


#Editorial
@require_http_methods(['GET'])
def listar_editoriales(request):
    editoriales = Editorial.objects.all()
    return render(request, 'libro/listar_editorial.html', {
        'editoriales': editoriales
    })

@require_http_methods(["POST", "GET"])
def crear_editorial(request):
    if request.method == 'POST':
        editorial_form = EditorialForm(request.POST)
        if editorial_form.is_valid():
            editorial_form.save()
            return redirect('libro:listar_editoriales')
    else:
        editorial_form = EditorialForm()
    return render(request, 'libro/crear_actualizar_editorial.html', {
        'editorial_form': editorial_form
    })

@require_http_methods(["POST", "GET"])
def actualizar_editorial(request, editorial_id):
    editorial_found = Editorial.objects.get(id = editorial_id)
    if request.method == 'GET':
        editorial_form = EditorialForm(instance = editorial_found)
    else:
        editorial_form = EditorialForm(request.POST, request.FILES, instance = editorial_found)
        if editorial_form.is_valid():
            editorial_form.save()
        return redirect('libro:listar_editoriales')
    return render(request, 'libro/crear_actualizar_editorial.html', {
        'editorial_form': editorial_form
    })

def eliminar_editorial(request, editorial_id):
    editorial_found = Editorial.objects.get(id = editorial_id)
    if request.method == 'GET':
        return render(request, 'libro/eliminar_editorial.html', {
        'editorial_found': editorial_found
        })
    if request.method == 'POST':
        editorial_found.delete()
        return redirect('libro:listar_editoriales')


#Libro
@require_http_methods(['GET'])
def listar_libros(request):
    libros = Libro.objects.all().prefetch_related('autor_id', 'editorial_id')
    return render(request, 'libro/listar_libro.html', {
        'libros': libros
    })

@require_http_methods(["POST", "GET"])
def crear_libro(request):
    if request.method == 'POST':
        libro_form = LibroForm(request.POST, request.FILES)
        if libro_form.is_valid():
            libro_form.save()
            return redirect('libro:listar_libros')
    else:
        libro_form = LibroForm()
    return render(request, 'libro/crear_actualizar_libro.html', {
        'libro_form': libro_form
    })

@require_http_methods(["POST", "GET"])
def actualizar_libro(request, libro_id):
    libro_found = Libro.objects.get(id = libro_id)
    if request.method == 'GET':
        libro_form = LibroForm(instance = libro_found)
    else:
        libro_form = LibroForm(request.POST, request.FILES, instance = libro_found)
        if libro_form.is_valid():
            libro_form.save()
        return redirect('libro:listar_libros')
    return render(request, 'libro/crear_actualizar_libro.html', {
        'libro_form': libro_form
    })

def eliminar_libro(request, libro_id):
    libro_found = Libro.objects.get(id = libro_id)
    if request.method == 'GET':
        return render(request, 'libro/eliminar_libro.html', {
        'libro_found': libro_found
        })
    if request.method == 'POST':
        libro_found.delete()
        return redirect('libro:listar_libros')


#Alquiler
@require_http_methods(['GET'])
def listar_alquileres(request):
    alquileres = Alquiler.objects.all()
    return render(request, 'libro/listar_alquiler.html', {
        'alquileres': alquileres
    })

@require_http_methods(["POST", "GET"])
def crear_alquiler(request):
    if request.method == 'POST':
        alquiler_form = AlquilerForm(request.POST)
        if alquiler_form.is_valid():
            alquiler_form.save()
            return redirect('libro:listar_alquileres')
    else:
        alquiler_form = AlquilerForm()
    return render(request, 'libro/crear_actualizar_alquiler.html', {
        'alquiler_form': alquiler_form
    })

@require_http_methods(["POST", "GET"])
def actualizar_alquiler(request, alquiler_id):
    alquiler_found = Alquiler.objects.get(id = alquiler_id)
    if request.method == 'GET':
        alquiler_form = AlquilerForm(instance = alquiler_found)
    else:
        alquiler_form = AlquilerForm(request.POST, instance = alquiler_found)
        if alquiler_form.is_valid():
            alquiler_form.save()
        return redirect('libro:listar_alquileres')
    return render(request, 'libro/crear_actualizar_alquiler.html', {
        'alquiler_form': alquiler_form
    })

def eliminar_alquiler(request, alquiler_id):
    alquiler_found = Alquiler.objects.get(id = alquiler_id)
    if request.method == 'GET':
        return render(request, 'libro/eliminar_alquiler.html', {
        'alquiler_found': alquiler_found
        })
    if request.method == 'POST':
        alquiler_found.delete()
        return redirect('libro:listar_alquileres')
